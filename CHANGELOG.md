# Nevadaish Color Theme - Change Log

## [0.0.7]

- update screenshot / readme to indicate add'l supported apps
- canonical layout reorg and fix image links

## [0.0.6]

- update readme and screenshot

## [0.0.5]

- fix manifest and pub WF

## [0.0.4]

- fix icon path in pkg

## [0.0.3]

- fix manifest repo links
- fix button contrasts

## [0.0.2]

- match minimap BG to editor gutter, activity bar:
"minimap.background": "#50ac9b"

## [0.0.1]

- Initial release

# Nevadaish Theme

Nevadaish color theme for Code-OSS based apps (VSCode, Codium, code.dev, AzureDataStudio, TheiaIDE and Positron).

Created by sjsepan.

**Enjoy!**

VSCode:
![./images/sjsepan-nevadaish_code.png](./images/sjsepan-nevadaish_code.png?raw=true "VSCode")
Codium:
![./images/sjsepan-nevadaish_codium.png](./images/sjsepan-nevadaish_codium.png?raw=true "Codium")
Code.Dev:
![./images/sjsepan-nevadaish_codedev.png](./images/sjsepan-nevadaish_codedev.png?raw=true "Code.Dev")
Azure Data Studio:
![./images/sjsepan-nevadaish_ads.png](./images/sjsepan-nevadaish_ads.png?raw=true "Azure Data Studio")
TheiaIDE:
![./images/sjsepan-nevadaish_theia.png](./images/sjsepan-nevadaish_theia.png?raw=true "TheiaIDE")
Positron:
![./images/sjsepan-nevadaish_positron.png](./images/sjsepan-nevadaish_positron.png?raw=true "Positron")

## Contact

Steve Sepan

<sjsepan@yahoo.com>

1/14/2025
